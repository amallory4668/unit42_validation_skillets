---

name: "PickAxe"
label: "PickAxe"

description: |
   PickAxe is a threat actor active since at least August 2017, and continues to remain active to this day. The adversary's goal is to install and maintain a popular cryptocurrency miner on the victim's machine. The miner in question is an open-source tool named XMRig that generates the Monero cryptocurrency. Malware is delivered via downloads through the popular Adfly advertisement platform. Users are often mislead into clicking on a malicious advertisement that results in the payload being delivered to the victim. Once installed, the malware leverages VBS scripts and redirection services, such as bitly, to ultimately download and execute XMRig. Over 15 million confirmed victims have been discovered to be infected in recent campaigns, with actual numbers likely to be between 30-45 million victims. The victims are found across the globe, with high concentrations in Thailand, Vietnam, Egypt, Indonesia, and Turkey.

type: pan_validation
labels:
    collection:
        - Unit42
        - Validation

variables:

snippets:
    
    - name: course-of-action--036c8397-0a18-4203-b5dd-76136fdd0715
      label: Ensure that User-ID is only enabled for internal trusted interfaces
      meta: 
        mitre_phases: execution, lateral-movement
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_enabled_zones
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
        - name: userid_internal_zones_only
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
      fail_message: |-
        Only enable the User-ID option for interfaces that are both internal and trusted. There is rarely a legitimate need to allow WMI probing (or any user-id identification) on an untrusted interface. The exception to this is identification of remote-access VPN users, who are identified as they connect.
      
    
    - name: course-of-action--21ce34c9-4220-41cf-85c7-bc289bb2c79d
      label: Ensure a Vulnerability Protection Profile is set to block attacks against critical and high vulnerabilities, and set to default on medium, low, and informational vulnerabilities
      meta: 
        mitre_phases: initial-access
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_vulnerability_profile_rules
          meta: 
            mitre_phases: initial-access
            product: Threat_Prevention
        - name: vulnerability_profile_blocking
          meta: 
            mitre_phases: initial-access
            product: Threat_Prevention
      fail_message: |-
        Configure a Vulnerability Protection Profile set to block attacks against any critical or high vulnerabilities, at minimum, and set to default on any medium, low, or informational vulnerabilities. Configuring an alert action for low and informational, instead of default, will produce additional information at the expense of greater log utilization.
      
    
    - name: course-of-action--2a511267-37cb-4f48-b6dd-9683393cae52
      label: Ensure that the User-ID service account does not have interactive logon rights
      meta: 
        mitre_phases: execution, lateral-movement
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_no_interactive_logon
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
      fail_message: |-
        Restrict the User-ID service account from interactively logging on to systems in the Active Directory domain.
      
    
    - name: course-of-action--2c886776-61fb-487c-a880-41f4b7195627
      label: Ensure that PAN-DB URL Filtering is used
      meta: 
        mitre_phases: command-and-control, execution, initial-access
        product: URL_Filtering
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_url_license_info
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: URL_Filtering
        - name: pandb_url_filtering
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: URL_Filtering
      fail_message: |-
        Configure the device to use PAN-DB URL Filtering instead of BrightCloud.
      
    
    - name: course-of-action--2f52c055-abe4-40a4-99f4-b486bb15edf5
      label: Ensure that 'Include/Exclude Networks' is used if User-ID is enabled
      meta: 
        mitre_phases: execution, lateral-movement
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_include_exclude_networks
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
        - name: userid_with_include_exclude
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
      fail_message: |-
        If User-ID is configured, use the Include/Exclude Networks section to limit the User-ID scope to operate only on trusted networks. There is rarely a legitimate need to allow WMI probing or other User identification on an untrusted network.
      
    
    - name: course-of-action--2fd9769c-5e50-4528-985f-a1d117329991
      label: Ensure that URL Filtering uses the action of “block” or “override” on the <enterprise approved value> URL categories
      meta: 
        mitre_phases: command-and-control, execution, initial-access
        product: URL_Filtering
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_url_filtering_block_override
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: URL_Filtering
        - name: url_filtering_block_or_override_test
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: URL_Filtering
      fail_message: |-
        Ideally, deciding which URL categories to block, and which to allow, is a joint effort between IT and another entity of authority within an organization—such as the legal department or administration. For most organizations, blocking or requiring an override on the following categories represents a minimum baseline: adult, hacking, command-and-control, copyright-infringement, extremism, malware, phishing, proxy-avoidance-and-anonymizers, and parked. Some organizations may add 'unknown' and 'dynamic-dns' to this list, at the expense of some support calls on those topics.
      
    
    - name: course-of-action--412566a3-28a8-4ff7-b4c5-4edd24d8febc
      label: Ensure that security policies restrict User-ID Agent traffic from crossing into untrusted zones
      meta: 
        mitre_phases: execution, lateral-movement
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_security_policies
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
        - name: userid_agent_no_untrust_zone
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
      fail_message: |-
        Create security policies to deny Palo Alto User-ID traffic originating from the interface configured for the UID Agent service that are destined to any untrusted zone.
      
    
    - name: course-of-action--49043cd0-4510-42ca-8de3-2c2f2bce2c16
      label: Ensure that the User-ID Agent has minimal permissions if User-ID is enabled
      meta: 
        mitre_phases: execution, lateral-movement
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_agent_min_permissions
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
      fail_message: |-
        If the integrated (on-device) User-ID Agent is utilized, the Active Directory account for the agent should only be a member of the Event Log Readers group, Distributed COM Users group, and Domain Users group. If the Windows User-ID agent is utilized, the Active Directory account for the agent should only be a member of the Event Log Readers group, Server Operators group, and Domain Users group.
      
    
    - name: course-of-action--49790670-d365-43f8-a906-8e45c3c80f63
      label: Ensure a secure Vulnerability Protection Profile is applied to all security rules allowing traffic
      meta: 
        mitre_phases: initial-access
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_vulnerability_profile_policies
          meta: 
            mitre_phases: initial-access
            product: Threat_Prevention
        - name: vulnerability_profile_in_policies
          meta: 
            mitre_phases: initial-access
            product: Threat_Prevention
      fail_message: |-
        For any security rule allowing traffic, apply a securely configured Vulnerability Protection Profile. Careful analysis of the target environment should be performed before implementing this configuration, as outlined by PAN’s “Threat Prevention Deployment Tech Note” in the references section.
      
    
    - name: course-of-action--55e53da8-b2ba-4bba-a641-c5c75a023bb5
      label: Ensure 'Security Policy' denying any/all traffic to/from IP addresses on Trusted Threat Intelligence Sources Exists
      meta: 
        mitre_phases: execution, lateral-movement, command-and-control, initial-access, impact
        product: NGFW
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_license_info
          meta: 
            mitre_phases: execution, lateral-movement, command-and-control, initial-access, impact
            product: NGFW
        - name: capture_security_policy_settings
          meta: 
            mitre_phases: execution, lateral-movement, command-and-control, initial-access, impact
            product: NGFW
        - name: security_policy_deny_threat_ips
          meta: 
            mitre_phases: execution, lateral-movement, command-and-control, initial-access, impact
            product: NGFW
      fail_message: |-
        Create a pair of security rules at the top of the security policies ruleset to block traffic to and from IP addresses known to be malicious.  Note: This recommendation (as written) requires a Palo Alto 'Active Threat License'. Third Party and Open Source Threat Intelligence Feeds can also be used for this purpose.
      
    
    - name: course-of-action--56f85f7c-65f3-4aba-ad2f-eb4175c79be0
      label: Ensure remote access capabilities for the User-ID service account are forbidden.
      meta: 
        mitre_phases: execution, lateral-movement
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_service_account
          meta: 
            mitre_phases: execution, lateral-movement
            product: NGFW
      fail_message: |-
        Restrict the User-ID service account’s ability to gain remote access into the organization. This capability could be made available through a variety of technologies, such as VPN, Citrix GoToMyPC, or TeamViewer. Remote services that integrate authentication with the organization’s Active Directory may unintentionally allow the User-ID service account to gain remote access.
      
    
    - name: course-of-action--5839cd6c-7897-43c4-82f3-5c03096a51c4
      label: Ensure passive DNS monitoring is set to enabled on all anti-spyware profiles in use
      meta: 
        mitre_phases: command-and-control, execution
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_passive_dns_monitoring
          meta: 
            mitre_phases: command-and-control, execution
            product: Threat_Prevention
        - name: enable_passive_DNS_monitoring
          meta: 
            mitre_phases: command-and-control, execution
            product: Threat_Prevention
      fail_message: |-
        Enable passive DNS monitoring within all anti-spyware profiles in use.
      
    
    - name: course-of-action--5e951942-0565-46f4-b09e-80426812b6b5
      label: Ensure that access to every URL is logged
      meta: 
        mitre_phases: execution, initial-access, command-and-control
        product: URL_Filtering
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_predefined_url_categories
          meta: 
            mitre_phases: execution, initial-access, command-and-control
            product: URL_Filtering
        - name: capture_url_category_actions
          meta: 
            mitre_phases: execution, initial-access, command-and-control
            product: URL_Filtering
        - name: url_filtering_log_all_categories
          meta: 
            mitre_phases: execution, initial-access, command-and-control
            product: URL_Filtering
      fail_message: |-
        URL filters should not specify any categories as `Allow Categories`.
      
    
    - name: course-of-action--645c6c45-116e-4265-98d2-e30f56325bc6
      label: Ensure that antivirus profiles are set to block on all decoders except 'imap' and 'pop3'
      meta: 
        mitre_phases: execution, initial-access, command-and-control
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_av_profile_action_settings
          meta: 
            mitre_phases: execution, initial-access, command-and-control
            product: Threat_Prevention
        - name: av_profile_block_all_decoders_test
          meta: 
            mitre_phases: execution, initial-access, command-and-control
            product: Threat_Prevention
      fail_message: |-
        Configure antivirus profiles to a value of 'block' for all decoders except imap and pop3 under both Action and WildFire Action. If required by the organization's email implementation, configure imap and pop3 decoders to 'alert' under both Action and WildFire Action.
      
    
    - name: course-of-action--79fd0d10-6272-4191-8b8d-9c72f19358d0
      label: Ensure that WildFire file size upload limits are maximized
      meta: 
        mitre_phases: initial-access, execution
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wf_size_testing
          meta: 
            mitre_phases: initial-access, execution
            product: Wildfire
        - name: wf_limit_pe_test
          meta: 
            mitre_phases: initial-access, execution
            product: Wildfire
      fail_message: |-
        Increase WildFire file size limits to the maximum file size supported by the environment. An organization with bandwidth constraints or heavy usage of unique files under a supported file type may require lower settings. The recommendations account for the CPU load on smaller platforms. If an organization consistently has CPU to spare, it's recommended to set some or all of these values to the maximum.
      
    
    - name: course-of-action--8190ac5a-8a5e-41bc-85dd-62e0f5cdd2ba
      label: Ensure a WildFire Analysis profile is enabled for all security policies
      meta: 
        mitre_phases: execution, initial-access
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_profile_policies
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
        - name: WF_profile_in_policies
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
      fail_message: |-
        Ensure that all files traversing the firewall are inspected by WildFire by setting a Wildfire file blocking profile on all security policies.
      
    
    - name: course-of-action--922b1227-c493-43f6-8b4a-ae6d6963eb8b
      label: Ensure 'SSL Inbound Inspection' is required for all untrusted traffic destined for servers using SSL or TLS
      meta: 
        mitre_phases: command-and-control
        product: NGFW
        
      include: panos_validations_decryption_settings
      include_variables: all
      include_snippets:
        - name: capture_ssl_inbound_inspection
          meta: 
            mitre_phases: command-and-control
            product: NGFW
        - name: ssl_inbound_inspection
          meta: 
            mitre_phases: command-and-control
            product: NGFW
      fail_message: |-
        Configure SSL Inbound Inspection for all untrusted traffic destined for servers using SSL or TLS.
      
    
    - name: course-of-action--9bcdca50-296d-4032-8639-dc8bc56077a3
      label: Ensure 'Service setting of ANY' in a security policy allowing traffic does not exist
      meta: 
        mitre_phases: impact, command-and-control, initial-access, execution, lateral-movement
        product: NGFW
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_security_policy_any_service
          meta: 
            mitre_phases: impact, command-and-control, initial-access, execution, lateral-movement
            product: NGFW
        - name: service_any_not_allowed_in_policy
          meta: 
            mitre_phases: impact, command-and-control, initial-access, execution, lateral-movement
            product: NGFW
      fail_message: |-
        Create security policies specifying application-default for the Service setting, in addition to the specific ports desired. The Service setting of `any` should not be used for any policies that allow traffic.
      
    
    - name: course-of-action--a1294d38-0f0d-4973-a949-f38cccbc7469
      label: Ensure an anti-spyware profile is configured to block on all spyware severity levels, categories, and threats
      meta: 
        mitre_phases: execution, command-and-control
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_anti-spyware_profile_settings
          meta: 
            mitre_phases: execution, command-and-control
            product: Threat_Prevention
        - name: spyware_profile_block_all
          meta: 
            mitre_phases: execution, command-and-control
            product: Threat_Prevention
      fail_message: |-
        If a single rule exists within the anti-spyware profile, configure it to block on any spyware severity level, any category, and any threat. If multiple rules exist within the anti-spyware profile, ensure all spyware categories, threats, and severity levels are set to be blocked. Additional rules may exist for packet capture or exclusion purposes.
      
    
    - name: course-of-action--a592acb0-98ba-4a8e-a5cf-2e78d2806e75
      label: Ensure all WildFire session information settings are enabled
      meta: 
        mitre_phases: execution, initial-access
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_session_settings
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
        - name: WF_session_info_enabled
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
      fail_message: |-
        Enable all options under Session Information Settings for WildFire.
      
    
    - name: course-of-action--a79d2365-61c7-48b9-b024-e2020b99bba0
      label: Ensure alerts are enabled for malicious files detected  by WildFire
      meta: 
        mitre_phases: execution, initial-access
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_alerts
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
        - name: WF_alerts_malicious_files
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
      fail_message: |-
        Configure WildFire to send an alert when a malicious or greyware file is detected. This alert could be sent by whichever means is preferable, including email, SNMP trap, or syslog message.  Alternatively, configure the WildFire cloud to generate alerts for malicious files. The cloud can generate alerts in addition to or instead of the local WildFire implementation. Note that the destination email address of alerts configured in the WildFire cloud portal is tied to the logged in account, and cannot be modified. Also, new systems added to the WildFire cloud portal will not be automatically set to email alerts.
      
    
    - name: course-of-action--ae18aab6-0a08-4c09-9a97-6c6ef58061a7
      label: Enable DNS Security in Anti-Spyware profile
      meta: 
        mitre_phases: command-and-control, execution, initial-access
        product: DNS_Security
        
      test: true
      not_implemented: true
      fail_message: Not implemented
    
    - name: course-of-action--b1f31bc6-40cd-4c64-805f-1680eda35801
      label: Ensure 'SSL Forward Proxy Policy' for traffic destined to the Internet is configured
      meta: 
        mitre_phases: command-and-control
        product: NGFW
        
      include: panos_validations_decryption_settings
      include_variables: all
      include_snippets:
        - name: capture_ssl_forward_proxy_policy
          meta: 
            mitre_phases: command-and-control
            product: NGFW
        - name: ssl_forward_proxy_policy
          meta: 
            mitre_phases: command-and-control
            product: NGFW
      fail_message: |-
        Configure SSL Forward Proxy for all traffic destined to the Internet. In most organizations, including all categories except `financial-services`, `government` and `health-and-medicine` is recommended.
      
    
    - name: course-of-action--b4ed79f9-b72c-4423-9a9c-f29134fda870
      label: Ensure that the Certificate used for Decryption is Trusted
      meta: 
        mitre_phases: command-and-control
        product: NGFW
        
      include: panos_validations_decryption_settings
      include_variables: all
      include_snippets:
        - name: capture_decryption_certs
          meta: 
            mitre_phases: command-and-control
            product: NGFW
        - name: decrypt_cert_trusted
          meta: 
            mitre_phases: command-and-control
            product: NGFW
      fail_message: |-
        The CA Certificate used for in-line HTTP Man in the Middle should be trusted by target users. For `SSL Forward Proxy` configurations, there are classes of users that need to be considered.  1: Users that are members of the organization, users of machines under control of the organization. For these people and machines, ensure that the CA Certificate is in one of the Trusted CA certificate stores. This is easily done in Active Directory, using Group Policies for instance. A MDM (Mobile Device Manager) can be used to accomplish the same task for mobile devices such as telephones or tablets. Other central management or orchestration tools can be used for Linux or 'IoT' (Internet of Things) devices.  2: Users that are not member of the organization - often these are classed as 'Visitors' in the policies of the organization. If a public CA Certificate is a possibility for your organization, then that is one approach. A second approach is to not decrypt affected traffic - this is easily done, but leaves the majority of 'visitor' traffic uninspected and potentially carrying malicious content. The final approach, and the one most commonly seen, is to use the same certificate as is used for the hosting organization. In this last case, visitors will see a certificate warning, but the issuing CA will be the organization that they are visiting.
      
    
    - name: course-of-action--befa473a-9ac8-4ff8-b0fe-be0e47821896
      label: Ensure a secure antivirus profile is applied to all relevant security policies
      meta: 
        mitre_phases: command-and-control, execution, initial-access
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_av_profile_in_policies
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: Threat_Prevention
        - name: AV_profile_in_policies
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: Threat_Prevention
      fail_message: |-
        Create a secure antivirus profile and apply it to all security policies that could pass HTTP, SMTP, IMAP, POP3, FTP, or SMB traffic. The antivirus profile may be applied to the security policies directly or through a profile group.
      
    
    - name: course-of-action--c45b79a8-2d29-4d8c-95ab-acea7e478e50
      label: Ensure DNS sinkholing is configured on all anti-spyware profiles in use
      meta: 
        mitre_phases: command-and-control, execution
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_dns_sinkhole_configuration
          meta: 
            mitre_phases: command-and-control, execution
            product: Threat_Prevention
        - name: DNS_sinkhole_configured
          meta: 
            mitre_phases: command-and-control, execution
            product: Threat_Prevention
      fail_message: |-
        Configure DNS sinkholing for all anti-spyware profiles in use. All internal requests to the selected sinkhole IP address must traverse the firewall. Any device attempting to communicate with the DNS sinkhole IP address should be considered infected.
      
    
    - name: course-of-action--d3c39d4c-3074-4350-832e-9eea942ac353
      label: Ensure application security policies exist when allowing traffic from an untrusted zone to a more trusted zone
      meta: 
        mitre_phases: impact, command-and-control, initial-access, execution, lateral-movement
        product: NGFW
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_app_security_in_policies
          meta: 
            mitre_phases: impact, command-and-control, initial-access, execution, lateral-movement
            product: NGFW
        - name: app_security_more_trusted_zone
          meta: 
            mitre_phases: impact, command-and-control, initial-access, execution, lateral-movement
            product: NGFW
      fail_message: |-
        When permitting traffic from an untrusted zone, such as the Internet or guest network, to a more trusted zone, such as a DMZ segment, create security policies specifying which specific applications are allowed.   **Enhanced Security Recommendation: ** Require specific application policies when allowing `any` traffic, regardless of the trust level of a zone. Do not rely solely on port permissions. This may require SSL interception, and may also not be possible in all environments.
      
    
    - name: course-of-action--d84f79f8-219c-43cb-a918-ab7dd235413a
      label: Ensure a secure anti-spyware profile is applied to all security policies permitting traffic to the Internet
      meta: 
        mitre_phases: command-and-control, execution
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_antispyware_profiles_in_policies
          meta: 
            mitre_phases: command-and-control, execution
            product: Threat_Prevention
        - name: spyware_profile_in_policies
          meta: 
            mitre_phases: command-and-control, execution
            product: Threat_Prevention
      fail_message: |-
        Create one or more anti-spyware profiles and collectively apply them to all security policies permitting traffic to the Internet. The anti-spyware profiles may be applied to the security policies directly or through a profile group.
      
    
    - name: course-of-action--dff22e97-a89e-4ff7-b615-0165258bf8d5
      label: Ensure all HTTP Header Logging options are enabled
      meta: 
        mitre_phases: command-and-control, initial-access, execution
        product: URL_Filtering
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_http_header_logging_options
          meta: 
            mitre_phases: command-and-control, initial-access, execution
            product: URL_Filtering
        - name: url_filtering_http_header_logging
          meta: 
            mitre_phases: command-and-control, initial-access, execution
            product: URL_Filtering
      fail_message: |-
        Enable all options (User-Agent, Referer, and X-Forwarded-For) for HTTP header logging.
      
    
    - name: course-of-action--ef5f68d8-9522-40b4-890c-31ece644687d
      label: Ensure 'WildFire Update Schedule' is set to download and install updates every minute
      meta: 
        mitre_phases: execution, initial-access
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_update_schedule
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
        - name: WF_update_schedule
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
      fail_message: |-
        Set the WildFire update schedule to download and install updates every minute.
      
    
    - name: course-of-action--f4a8da12-c6f0-4a33-ae42-590c5602be99
      label: Ensure secure URL filtering is enabled for all security policies allowing traffic to the Internet
      meta: 
        mitre_phases: command-and-control, execution, initial-access
        product: URL_Filtering
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_url_filtering_in_policies
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: URL_Filtering
        - name: urlfiltering_profile_in_policies
          meta: 
            mitre_phases: command-and-control, execution, initial-access
            product: URL_Filtering
      fail_message: |-
        Apply a secure URL filtering profile to all security policies permitting traffic to the Internet. The URL Filtering profile may be applied to the security policies directly or through a profile group.
      
    
    - name: course-of-action--f4d50de0-ecc5-4f9e-b8f7-214cdfe28d39
      label: Ensure forwarding is enabled for all applications and file types in WildFire file blocking profiles
      meta: 
        mitre_phases: execution, initial-access
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_profile_settings
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
        - name: WF_all_apps_files
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
      fail_message: |-
        Set Applications and File Types fields to any in WildFire file blocking profiles. With a WildFire license, seven file types are supported, while only PE (Portable Executable) files are supported without a license. For the 'web browsing' application, the action 'continue' can be selected. This still forwards the file to the Wildfire service, but also presents the end user with a confirmation message before they receive the file. Selecting 'continue' for any other application will block the file (because the end user will not see the prompt). If there is a 'continue' rule, there should still be an 'any traffic / any application / forward' rule after that in the list.
      
    
    - name: course-of-action--fd0da09e-a0b2-4018-9476-1a7edd809b59
      label: Ensure forwarding of decrypted content to WildFire is enabled
      meta: 
        mitre_phases: execution, initial-access
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_allow_forwarding_decrypted_content
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
        - name: WF_forward_decrypt_content
          meta: 
            mitre_phases: execution, initial-access
            product: Wildfire
      fail_message: |-
        Allow the firewall to forward decrypted content to WildFire. Note that SSL Forward-Proxy must also be enabled and configured for this setting to take effect on inside-to-outside traffic flows.
      
    