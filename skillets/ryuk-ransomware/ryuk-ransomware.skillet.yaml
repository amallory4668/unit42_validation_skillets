---

name: "Ryuk Ransomware"
label: "Ryuk Ransomware"

description: |
   Ryuk is a ransomware designed to target enterprise environments that has been used in attacks since at least 2018. Ryuk shares code similarities with Hermes ransomware.

type: pan_validation
labels:
    collection:
        - Unit42
        - Validation

variables:

snippets:
    
    - name: course-of-action--09cad5e4-8c95-494f-862e-0c640b175348
      label: Ensure that security policies restrict User-ID Agent traffic from crossing into untrusted zones
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_security_policies
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
        - name: userid_agent_no_untrust_zone
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Create security policies to deny Palo Alto User-ID traffic originating from the interface configured for the UID Agent service that are destined to any untrusted zone.
      
    
    - name: course-of-action--39928312-81bb-4445-a269-9f3d0bb88d5c
      label: Ensure that the User-ID service account does not have interactive logon rights
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_no_interactive_logon
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Restrict the User-ID service account from interactively logging on to systems in the Active Directory domain.
      
    
    - name: course-of-action--553711d6-06e4-49e2-a1ad-929d9cce8e39
      label: Ensure that 'Include/Exclude Networks' is used if User-ID is enabled
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_include_exclude_networks
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
        - name: userid_with_include_exclude
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        If User-ID is configured, use the Include/Exclude Networks section to limit the User-ID scope to operate only on trusted networks. There is rarely a legitimate need to allow WMI probing or other User identification on an untrusted network.
      
    
    - name: course-of-action--66779efa-ecc3-4e80-91b9-c584b171ebe6
      label: Ensure that User Credential Submission uses the action of “block” or “continue” on the URL categories
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_predefined_url_categories
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: capture_url_filtering_user_credential
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: user_cred_block_continue
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
      fail_message: |-
        Ideally user names and passwords user within an organization are not used with third party sites. Some sanctioned SAS applications may have connections to the corporate domain, in which case they will need to be exempt from the user credential submission policy through a custom URL category.
      
    
    - name: course-of-action--68c5676d-ab2f-4e68-a57c-28880a9f5709
      label: Ensure that the User-ID Agent has minimal permissions if User-ID is enabled
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_agent_min_permissions
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        If the integrated (on-device) User-ID Agent is utilized, the Active Directory account for the agent should only be a member of the Event Log Readers group, Distributed COM Users group, and Domain Users group. If the Windows User-ID agent is utilized, the Active Directory account for the agent should only be a member of the Event Log Readers group, Server Operators group, and Domain Users group.
      
    
    - name: course-of-action--7bbb332f-22cf-48f2-a4a1-5bc9d2b034ca
      label: Ensure that antivirus profiles are set to block on all decoders except 'imap' and 'pop3'
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_av_profile_action_settings
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: av_profile_block_all_decoders_test
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
      fail_message: |-
        Configure antivirus profiles to a value of 'block' for all decoders except imap and pop3 under both Action and WildFire Action. If required by the organization's email implementation, configure imap and pop3 decoders to 'alert' under both Action and WildFire Action.
      
    
    - name: course-of-action--b07fe965-d582-4581-aa2f-9676705f5560
      label: Ensure remote access capabilities for the User-ID service account are forbidden.
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_service_account
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Restrict the User-ID service account’s ability to gain remote access into the organization. This capability could be made available through a variety of technologies, such as VPN, Citrix GoToMyPC, or TeamViewer. Remote services that integrate authentication with the organization’s Active Directory may unintentionally allow the User-ID service account to gain remote access.
      
    
    - name: course-of-action--b62423f4-1849-4d9f-88c0-7160a8bed5c2
      label: Ensure that User-ID is only enabled for internal trusted interfaces
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_enabled_zones
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
        - name: userid_internal_zones_only
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Only enable the User-ID option for interfaces that are both internal and trusted. There is rarely a legitimate need to allow WMI probing (or any user-id identification) on an untrusted interface. The exception to this is identification of remote-access VPN users, who are identified as they connect.
      
    
    - name: course-of-action--e15f23e0-f6ae-4440-865f-63ac76b93bf5
      label: Ensure a secure antivirus profile is applied to all relevant security policies
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_av_profile_in_policies
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: AV_profile_in_policies
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
      fail_message: |-
        Create a secure antivirus profile and apply it to all security policies that could pass HTTP, SMTP, IMAP, POP3, FTP, or SMB traffic. The antivirus profile may be applied to the security policies directly or through a profile group.
      
    