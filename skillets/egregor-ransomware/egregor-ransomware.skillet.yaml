---

name: "Egregor Ransomware"
label: "Egregor Ransomware"

description: |
   Egregor is a ransomware from the Sekhment malware family that has been active since september. This ransomware group prioritizes stealing data to use as additional extortion when files are encrypted. Egregor contains anti-analysis techniques such as code obfuscation and packed payloads .Also, in one of the execution stages, the Egregor payload can only be decrypted if the correct key is provided in the process' command line, meaning that the file cannot be analyzed, either manually or using a sandbox, if the correct key is not provided.

type: pan_validation
labels:
    collection:
        - Unit42
        - Validation

variables:

snippets:
    
    - name: course-of-action--04a2db1c-3e80-43a4-a9c6-3864195bbf73
      label: Ensure alerts are enabled for malicious files detected  by WildFire
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation, execution
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_profile_settings
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation, execution
            product: Wildfire
        - name: WF_all_apps_files
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation, execution
            product: Wildfire
      fail_message: |-
        Configure WildFire to send an alert when a malicious or greyware file is detected. This alert could be sent by whichever means is preferable, including email, SNMP trap, or syslog message.  Alternatively, configure the WildFire cloud to generate alerts for malicious files. The cloud can generate alerts in addition to or instead of the local WildFire implementation. Note that the destination email address of alerts configured in the WildFire cloud portal is tied to the logged in account, and cannot be modified. Also, new systems added to the WildFire cloud portal will not be automatically set to email alerts.
      
    
    - name: course-of-action--09cad5e4-8c95-494f-862e-0c640b175348
      label: Ensure that security policies restrict User-ID Agent traffic from crossing into untrusted zones
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_security_policies
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
        - name: userid_agent_no_untrust_zone
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Create security policies to deny Palo Alto User-ID traffic originating from the interface configured for the UID Agent service that are destined to any untrusted zone.
      
    
    - name: course-of-action--39928312-81bb-4445-a269-9f3d0bb88d5c
      label: Ensure that the User-ID service account does not have interactive logon rights
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_no_interactive_logon
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Restrict the User-ID service account from interactively logging on to systems in the Active Directory domain.
      
    
    - name: course-of-action--3de85a76-a879-43e6-80ba-38e09e7e2b0c
      label: Ensure a WildFire Analysis profile is enabled for all security policies
      meta: 
        mitre_phases: initial-access, execution, defense-evasion, persistence, privilege-escalation
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_profile_policies
          meta: 
            mitre_phases: initial-access, execution, defense-evasion, persistence, privilege-escalation
            product: Wildfire
        - name: WF_profile_in_policies
          meta: 
            mitre_phases: initial-access, execution, defense-evasion, persistence, privilege-escalation
            product: Wildfire
      fail_message: |-
        Ensure that all files traversing the firewall are inspected by WildFire by setting a Wildfire file blocking profile on all security policies.
      
    
    - name: course-of-action--49881f38-2571-47a1-a122-26a5968f1137
      label: Ensure 'Service setting of ANY' in a security policy allowing traffic does not exist
      meta: 
        mitre_phases: discovery
        product: NGFW
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_security_policy_any_service
          meta: 
            mitre_phases: discovery
            product: NGFW
        - name: service_any_not_allowed_in_policy
          meta: 
            mitre_phases: discovery
            product: NGFW
      fail_message: |-
        Create security policies specifying application-default for the Service setting, in addition to the specific ports desired. The Service setting of `any` should not be used for any policies that allow traffic.
      
    
    - name: course-of-action--553711d6-06e4-49e2-a1ad-929d9cce8e39
      label: Ensure that 'Include/Exclude Networks' is used if User-ID is enabled
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_include_exclude_networks
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
        - name: userid_with_include_exclude
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        If User-ID is configured, use the Include/Exclude Networks section to limit the User-ID scope to operate only on trusted networks. There is rarely a legitimate need to allow WMI probing or other User identification on an untrusted network.
      
    
    - name: course-of-action--66779efa-ecc3-4e80-91b9-c584b171ebe6
      label: Ensure that User Credential Submission uses the action of “block” or “continue” on the URL categories
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_predefined_url_categories
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: capture_url_filtering_user_credential
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: user_cred_block_continue
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
      fail_message: |-
        Ideally user names and passwords user within an organization are not used with third party sites. Some sanctioned SAS applications may have connections to the corporate domain, in which case they will need to be exempt from the user credential submission policy through a custom URL category.
      
    
    - name: course-of-action--67289170-1bd4-4944-be31-d680954141f5
      label: Ensure 'WildFire Update Schedule' is set to download and install updates every minute
      meta: 
        mitre_phases: initial-access, defense-evasion, persistence, privilege-escalation, execution
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_update_schedule
          meta: 
            mitre_phases: initial-access, defense-evasion, persistence, privilege-escalation, execution
            product: Wildfire
        - name: WF_update_schedule
          meta: 
            mitre_phases: initial-access, defense-evasion, persistence, privilege-escalation, execution
            product: Wildfire
      fail_message: |-
        Set the WildFire update schedule to download and install updates every minute.
      
    
    - name: course-of-action--68c5676d-ab2f-4e68-a57c-28880a9f5709
      label: Ensure that the User-ID Agent has minimal permissions if User-ID is enabled
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_agent_min_permissions
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        If the integrated (on-device) User-ID Agent is utilized, the Active Directory account for the agent should only be a member of the Event Log Readers group, Distributed COM Users group, and Domain Users group. If the Windows User-ID agent is utilized, the Active Directory account for the agent should only be a member of the Event Log Readers group, Server Operators group, and Domain Users group.
      
    
    - name: course-of-action--7bbb332f-22cf-48f2-a4a1-5bc9d2b034ca
      label: Ensure that antivirus profiles are set to block on all decoders except 'imap' and 'pop3'
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_av_profile_action_settings
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: av_profile_block_all_decoders_test
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
      fail_message: |-
        Configure antivirus profiles to a value of 'block' for all decoders except imap and pop3 under both Action and WildFire Action. If required by the organization's email implementation, configure imap and pop3 decoders to 'alert' under both Action and WildFire Action.
      
    
    - name: course-of-action--95432623-8819-4f74-a8f9-b10b9c4118c3
      label: Ensure forwarding is enabled for all applications and file types in WildFire file blocking profiles
      meta: 
        mitre_phases: defense-evasion, execution, initial-access, persistence, privilege-escalation
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_profile_settings
          meta: 
            mitre_phases: defense-evasion, execution, initial-access, persistence, privilege-escalation
            product: Wildfire
        - name: WF_all_apps_files
          meta: 
            mitre_phases: defense-evasion, execution, initial-access, persistence, privilege-escalation
            product: Wildfire
      fail_message: |-
        Set Applications and File Types fields to any in WildFire file blocking profiles. With a WildFire license, seven file types are supported, while only PE (Portable Executable) files are supported without a license. For the 'web browsing' application, the action 'continue' can be selected. This still forwards the file to the Wildfire service, but also presents the end user with a confirmation message before they receive the file. Selecting 'continue' for any other application will block the file (because the end user will not see the prompt). If there is a 'continue' rule, there should still be an 'any traffic / any application / forward' rule after that in the list.
      
    
    - name: course-of-action--b07fe965-d582-4581-aa2f-9676705f5560
      label: Ensure remote access capabilities for the User-ID service account are forbidden.
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: userid_service_account
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Restrict the User-ID service account’s ability to gain remote access into the organization. This capability could be made available through a variety of technologies, such as VPN, Citrix GoToMyPC, or TeamViewer. Remote services that integrate authentication with the organization’s Active Directory may unintentionally allow the User-ID service account to gain remote access.
      
    
    - name: course-of-action--b62423f4-1849-4d9f-88c0-7160a8bed5c2
      label: Ensure that User-ID is only enabled for internal trusted interfaces
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: NGFW
        
      include: panos_validations_userid_settings
      include_variables: all
      include_snippets:
        - name: capture_userid_enabled_zones
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
        - name: userid_internal_zones_only
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: NGFW
      fail_message: |-
        Only enable the User-ID option for interfaces that are both internal and trusted. There is rarely a legitimate need to allow WMI probing (or any user-id identification) on an untrusted interface. The exception to this is identification of remote-access VPN users, who are identified as they connect.
      
    
    - name: course-of-action--bca382b3-2a97-451f-a849-80f7a4e7edce
      label: Ensure 'Security Policy' denying any/all traffic to/from IP addresses on Trusted Threat Intelligence Sources Exists
      meta: 
        mitre_phases: discovery
        product: NGFW
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_license_info
          meta: 
            mitre_phases: discovery
            product: NGFW
        - name: capture_security_policy_settings
          meta: 
            mitre_phases: discovery
            product: NGFW
        - name: security_policy_deny_threat_ips
          meta: 
            mitre_phases: discovery
            product: NGFW
      fail_message: |-
        Create a pair of security rules at the top of the security policies ruleset to block traffic to and from IP addresses known to be malicious.  Note: This recommendation (as written) requires a Palo Alto 'Active Threat License'. Third Party and Open Source Threat Intelligence Feeds can also be used for this purpose.
      
    
    - name: course-of-action--be1d0eb5-a628-4d91-9096-9158e68816cd
      label: Ensure all WildFire session information settings are enabled
      meta: 
        mitre_phases: defense-evasion, persistence, privilege-escalation, execution, initial-access
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_session_settings
          meta: 
            mitre_phases: defense-evasion, persistence, privilege-escalation, execution, initial-access
            product: Wildfire
        - name: WF_session_info_enabled
          meta: 
            mitre_phases: defense-evasion, persistence, privilege-escalation, execution, initial-access
            product: Wildfire
      fail_message: |-
        Enable all options under Session Information Settings for WildFire.
      
    
    - name: course-of-action--dcd486e6-fcf5-4692-9187-115efdae9694
      label: Ensure application security policies exist when allowing traffic from an untrusted zone to a more trusted zone
      meta: 
        mitre_phases: discovery
        product: NGFW
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_app_security_in_policies
          meta: 
            mitre_phases: discovery
            product: NGFW
        - name: app_security_more_trusted_zone
          meta: 
            mitre_phases: discovery
            product: NGFW
      fail_message: |-
        When permitting traffic from an untrusted zone, such as the Internet or guest network, to a more trusted zone, such as a DMZ segment, create security policies specifying which specific applications are allowed.   **Enhanced Security Recommendation: ** Require specific application policies when allowing `any` traffic, regardless of the trust level of a zone. Do not rely solely on port permissions. This may require SSL interception, and may also not be possible in all environments.
      
    
    - name: course-of-action--e15f23e0-f6ae-4440-865f-63ac76b93bf5
      label: Ensure a secure antivirus profile is applied to all relevant security policies
      meta: 
        mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
        product: Threat_Prevention
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_av_profile_in_policies
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
        - name: AV_profile_in_policies
          meta: 
            mitre_phases: defense-evasion, initial-access, persistence, privilege-escalation
            product: Threat_Prevention
      fail_message: |-
        Create a secure antivirus profile and apply it to all security policies that could pass HTTP, SMTP, IMAP, POP3, FTP, or SMB traffic. The antivirus profile may be applied to the security policies directly or through a profile group.
      
    
    - name: course-of-action--f1a1d494-3463-4067-abc7-a731f7dfb9ff
      label: Ensure that WildFire file size upload limits are maximized
      meta: 
        mitre_phases: defense-evasion, persistence, privilege-escalation, initial-access, execution
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wf_size_testing
          meta: 
            mitre_phases: defense-evasion, persistence, privilege-escalation, initial-access, execution
            product: Wildfire
        - name: wf_limit_pe_test
          meta: 
            mitre_phases: defense-evasion, persistence, privilege-escalation, initial-access, execution
            product: Wildfire
      fail_message: |-
        Increase WildFire file size limits to the maximum file size supported by the environment. An organization with bandwidth constraints or heavy usage of unique files under a supported file type may require lower settings. The recommendations account for the CPU load on smaller platforms. If an organization consistently has CPU to spare, it's recommended to set some or all of these values to the maximum.
      
    
    - name: course-of-action--fafab9a4-1478-499e-9088-2043c42720d1
      label: Ensure forwarding of decrypted content to WildFire is enabled
      meta: 
        mitre_phases: defense-evasion, execution, initial-access, persistence, privilege-escalation
        product: Wildfire
        
      include: panos_validations_profile_objects
      include_variables: all
      include_snippets:
        - name: capture_wildfire_allow_forwarding_decrypted_content
          meta: 
            mitre_phases: defense-evasion, execution, initial-access, persistence, privilege-escalation
            product: Wildfire
        - name: WF_forward_decrypt_content
          meta: 
            mitre_phases: defense-evasion, execution, initial-access, persistence, privilege-escalation
            product: Wildfire
      fail_message: |-
        Allow the firewall to forward decrypted content to WildFire. Note that SSL Forward-Proxy must also be enabled and configured for this setting to take effect on inside-to-outside traffic flows.
      
    